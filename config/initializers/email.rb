if ActionMailer::Base.delivery_method == :smtp
  ActionMailer::Base.smtp_settings[:domain] = Rails.application.secrets.smtp_domain
  ActionMailer::Base.smtp_settings[:admin_name] = Rails.application.secrets.smtp_admin_name
  ActionMailer::Base.smtp_settings[:password] = Rails.application.secrets.smtp_password
end