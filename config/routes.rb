Rails.application.routes.draw do
  root 'freelancers#index'

  resources :freelancers do
    member do
      get :confirm_email
      get :expired
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
