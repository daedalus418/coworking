class ApplicationMailer < ActionMailer::Base
  default from: 'morin.gaelle@gmail.com'
  layout 'mailer'
end
