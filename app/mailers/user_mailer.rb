class UserMailer < ApplicationMailer
    #default from: 'notifications@example.com'

    def confirmation_email(freelancer)
        @freelancer = freelancer
        mail to: freelancer.email, subject: 'Enregistrez-vous dans notre incroyable file d\'attente !'
    end

    def contract_email(freelancer)
        @freelancer = freelancer
        mail to: freelancer.email, subject: 'Contrat'
    end

    def info_email(freelancer)
        @freelancer = freelancer
        mail to: freelancer.email, subject: 'Infos à confirmer'
    end
end
