class Freelancer < ApplicationRecord

    before_create :set_confirmation_token

    validates :name,
                presence: true

    validates :email,
                presence: true,
                format: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

    validates :phone,
                presence: true

    validates :bio,
                presence: true

    enum status: [:confirm, :accept, :expired, :unconfirmed]
    after_initialize :set_default_status


    belongs_to :coworking_area, optional: true

    def validate_email
      self.email_confirmed = true
      self.confirm_token = nil
      self.status = :confirm
      self.save
    end

    private

    def set_default_status
      self.status ||= :unconfirmed
    end

    def set_confirmation_token
      if self.confirm_token.blank?
        self.confirm_token = SecureRandom.urlsafe_base64.to_s
      end
    end

end
