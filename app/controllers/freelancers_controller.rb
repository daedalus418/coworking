class FreelancersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @freelancers = Freelancer.all
  end

  def new
    @freelancer = Freelancer.new
  end

  def create
    @freelancer = Freelancer.new(freelancer_params)

    if @freelancer.save
      logger.debug ENV["HEROSENDGRID_USERNAME"]
      logger.debug ENV["HEROSENDGRID_PASSWORD"]
      UserMailer.confirmation_email(@freelancer).deliver_now
      redirect_to root_url, notice: 'Votre demande est enregistrée, confirmez votre email.'
    else
      render :new
    end
  end

  def confirm_email
    freelancer = Freelancer.find_by(confirm_token: params[:confirm_token])
    @freelancer = freelancer
    if @freelancer
      @freelancer.validate_email
      redirect_to root_url
    else
      redirect_to root_url
    end
  end

  def edit
    freelancer = Freelancer.find_by(id: params[:id])
    @freelancer = freelancer
  end

  def update
    @freelancer = Freelancer.find_by(id: params[:id])

    if @freelancer.update(freelancer_params)
      if Freelancer.accept.size < 4
        redirect_to root_path, notice: 'Vos infos ont été mises à jour.'
      else
        @freelancer.confirm!
        redirect_to root_path, notice: 'Nope!'
      end
    else
      render :edit
    end
  end

  def destroy
    freelancer = Freelancer.find_by(id: params[:id])
    freelancer.destroy
    redirect_to root_path, notice: 'Ce freelancer est retiré de la liste.'
  end

  def expired
    @freelancer = Freelancer.find_by(id: params[:id])
    @freelancer.expired!
    redirect_to root_path, notice: 'Ce freelancer s\'est retiré de la liste d\'attente.'
  end

  private

  def freelancer_params
    params.require(:freelancer).permit(:name, :email, :phone, :bio, :status)
  end
end
