desc 'send contract email to each freelancer in Freelance.accept'
task :send_contract_email => :environment do |_, args|
    Freelancer.accept.find_each do |freelancer|
        UserMailer.contract_email(freelancer).deliver_now
    end
end

desc 'send info email to each freelancer in Freelance.confirm'
task :send_info_email => :environment do |_, args|
    Freelancer.confirm.find_each do |freelancer|
        UserMailer.info_email(freelancer).deliver_now
    end
end

